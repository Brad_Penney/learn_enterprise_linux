---
title: "Basic Toolset for Enterprise Linux"
date: 2023-03-12T19:25:47-03:00
draft: false 
---

## The Self-Documenting Operating System
One of the best things about Linux is that it is primarily self-documented. If someone is familiar with all the ways to find help within Linux (assuming sufficient patience and time), the operating system itself will likely provide the answers.

[Finding Help in Linux]({{< ref "/findingHelpInLinux.md" >}})

[Generating the Manual]({{< ref "/generatingManPages.md" >}})

[Linux File System Hierarchy]({{< ref "/linuxFilesystemHierarchy.md" >}})

## Command Line Basics
The Command Line (CLI) may be intimidating for those from a more Graphical User Interface (GUI) background. However, many users eventually find that the CLI is actually a very comfortable and powerful way to get difficult things done!

[Enterprise Command Line Shells]({{< ref "/shells.md" >}})

[Using the Command Line]({{< ref "/usingTheCommandLine.md" >}})

[BASH Shortcuts]({{< ref "/BASHShortcuts.md" >}})

[Shell History]({{< ref "/shellHistory.md" >}})

## Manipulating Files
In Linux, everything is a file. Effectively viewing and manipulating files is a crucial skill that unlocks all the other goodness that Linux has to offer.

[Command Line Text Editors]({{< ref "/textEditors.md" >}})

[File Pagers]({{< ref "/pagers.md" >}})
